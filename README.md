# PLUTO Language Plugin for Atom IDE

This is a plugin for the domain-specific programming language PLUTO defined by
ECSS to make it easier to write and read space mission operations procedures.

The PLUTO language is defined [here](docs/ECSS-E-ST-70-32C31July2008.pdf).


## Installation

(Not yet available)

To install:

- Go to Settings -> Install
- Type "language-pluto"
- Press "Install"

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/language-pluto/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/language-pluto

To contribue to the development of this package:

- Clone repository to your local package folder, typically /home/username/.atom/packages
- Restart Atom IDE with View -> Developer -> Reload Window (also to be done everytime when you modify your package)
- Add new rules! Use the PLUTO example files in the /examples folder to try it out.

Helpful resources on package writing:

- [How To Develop a Package for Atom Code Editor](https://www.sitepoint.com/how-to-write-a-syntax-highlighting-package-for-atom/)
- [Grammars naming conventions](https://macromates.com/manual/en/language_grammars#naming_conventions)
- [A guide to writing a language grammar (TextMate) in Atom](https://gist.github.com/Aerijo/b8c82d647db783187804e86fa0a604a1)

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/!DYAKaIbRbFKLWmKDNn:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
